TransientStorage
=============

This plugin is created to ease development of applications using session data by extending the [Wordpress Transients API](https://codex.wordpress.org/Transients_API)

**PHP Version Requirement:** This Plugin shouldn't be used with PHP-versions lower than 7.1

## Usage

**Create a Session-based transient**
```php
/**
 * @param $name Name of the cookie
 * @param $data Data to store
 * 
 * @return bool|\WP_ERROR True on success, else WP_ERROR Containing error data 
 */
\TransientStorage\createTransientSession(string $name, $data);
```

**Retrieve a Session-based transient**
```php
/**
 * @param $name Name of the cookie
 * 
 * @return mixed|\WP_ERROR Transient data or \WP_ERROR on failure
 */
\TransientStorage\getTransientSession(string $name);
```

**Update a Session-based transient**
```php
/**
 * @param $name Name of the cookie
 * @param $data Data to update
 * 
 * @return bool|\WP_ERROR True on success, else WP_ERROR Containing error data 
 */
\TransientStorage\updateTransientSession(string $name, $data);
```

**Delete a Session-based transient**
```php
/**
 * @param $name Name of the cookie
 * 
 * @return bool|\WP_ERROR True on success, else WP_ERROR Containing error data 
 */
\TransientStorage\deleteTransientSession(string $name);
```

## How it works

The principle behind this plugin is to store session data in the database instead of in the php session or cookie. This works by creating a database entry with an ID and storing the ID in a cookie. The cookie never has to be modified, only read, which avoids a lot of potential bugs.

### Advantages of this approach

This approach removes the need of using php-sessions entirely. Sessions are insecure and have a lot of problems when running your wordpress application in the cloud. Most object-cache systems have problems with data stored in sessions, but this approach sidesteps that problem completely.

### Other possible use-cases

Since this plugin uses the Wordpress Transients API, it's possible to use it to cache anything you desire. This plugin provides a safe and reliable way to use transients.

Transients should be used to store slow queris, slow api calls and similar actions.

## Development

Contributions are highly appriciated. Create pull requests for any issue or feature you might have fixed.

This plugin uses the PSR-2 coding standard.


## Contributors
This plugin was developed by Jakob Råhlén with the support of the team at [QTE Development AB](https://getqte.se/)


## License
TransientStorage is free software; you can redistribute it and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl-2.0.html) as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.