<?php

/*
 * Plugin Name: Transient Storage
 * Description: Wrapper library for transient storage with support for cookies
 * Version: 1.0.0
 * Author: Jakob Råhlén
 * Author URI: https://getqte.se/
 */

namespace TransientStorage;

use StorageHandlers\CookieHandler;
use StorageHandlers\TransientHandler;

if (!defined('ABSPATH')) {
    exit;
}

define('TRANSIENT_STORAGE_VERSION', '1.0.0');
define( 'TRANSIENT_STORAGE_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
define( 'TRANSIENT_STORAGE_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once TRANSIENT_STORAGE_PLUGIN_PATH . '/src/CookieHandler.php';
require_once TRANSIENT_STORAGE_PLUGIN_PATH . '/src/TransientHandler.php';
require_once TRANSIENT_STORAGE_PLUGIN_PATH . '/src/Rest.php';

/**
 * Initiate Rest
 *
 * @return void
 */
function restInit()
{
    $rest = new API\Rest();
}

\add_action( 'rest_api_init', __NAMESPACE__ . '\\restInit');

/**
 * Create transient and session
 *
 * @param string $name Cookie name
 * @param mixed $data  Transient Data
 *
 * @return bool|\WP_ERROR True on success, else WP_ERROR
 */
function createTransientSession(string $name, $data)
{
    $uniqueID = (string)\uniqid();

    $transient = TransientHandler::CREATE($uniqueID, $data);

    if (\is_wp_error($transient)) {
        return $transient;
    }

    $cookie = CookieHandler::CREATE($name, $uniqueID, 0);

    if (\is_wp_error($cookie)) {
        return $cookie;
    }

    return true;
}

/**
 * Update transient
 *
 * @param string $name Cookie name
 * @param mixed $data  New transient data
 *
 * @return bool|\WP_ERROR True on success, else WP_ERROR
 */
function updateTransientSession(string $name, $data)
{
    $cookieData = CookieHandler::FETCH($name);
    if (\is_wp_error($cookieData)) {
        return $cookieData;
    }

    $transient = TransientHandler::UPDATE($cookieData, $data);
    if (\is_wp_error($transient)) {
        return $transient;
    }

    return true;
}

/**
 * Delete transient and cookie
 *
 * @param string $name Cookie name
 *
 * @return bool|\WP_ERROR True on success, else WP_ERROR
 */
function deleteTransientSession(string $name)
{
    $cookieData = CookieHandler::FETCH($name);
    if (\is_wp_error($cookieData)) {
        return $cookieData;
    }

    $transient = TransientHandler::DELETE($cookieData);
    if (\is_wp_error($transient)) {
        return $transient;
    }

    $deleteCookie = CookieHandler::DELETE($name);
    if (\is_wp_error($deleteCookie)) {
        return $deleteCookie;
    }

    return true;
}

/**
 * Get transient data for cookie name
 *
 * @param string $name Cookie name
 *
 * @return mixed|\WP_ERROR Transient data or \WP_ERROR on failure
 */
function getTransientSession(string $name)
{
    $cookieData = CookieHandler::FETCH($name);
    if (\is_wp_error($cookieData)) {
        return $cookieData;
    }

    $transientData = TransientHandler::FETCH($cookieData);
    if (\is_wp_error($transientData)) {
        return $transientData;
    }

    return $transientData;
}