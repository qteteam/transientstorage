<?php

namespace TransientStorage\StorageHandlers;

class CookieHandler
{

    /**
     * Create cookie
     *
     * @param string $cookieName  Name to create cookio on
     * @param string $data        Data to store in cookie
     * @param integer $expiration Expiration in seconds
     * @param string $base        Base url
     *
     * @return bool|WP_ERROR True on success, WP_ERROR on failure
     */
    public static function CREATE(string $cookieName, string $data, int $expiration, string $base = '/')
    {
        $success = \setcookie($cookieName, $data, $expiration, $base);
        if (!$success) {
            return new \WP_ERROR(500, 'Cookie could not be created', $cookieName);
        }

        $_COOKIE[$cookieName] = $data;

        return true;
    }

    /**
     * Retrieve data from cookie
     *
     * @param string $cookieName Cookie to retrieve data from
     *
     * @return string cookie data
     */
    public static function FETCH(string $cookieName)
    {
        if (!isset($_COOKIE[$cookieName])) {
            return new \WP_ERROR(404, 'Cookie with name was not found', $cookieName);
        }
        $data = (string)$_COOKIE[$cookieName];

        return $data;
    }

    /**
     * Update cookie data
     *
     * @param string $cookieName Cookie to update
     * @param mixed $data        New data
     *
     * @return bool true on success
     */
    public static function UPDATE(string $cookieName, $data) : bool
    {
        $_COOKIE[$cookieName] = $data;

        return true;
    }

    /**
     * Delete cookie
     *
     * @param string $cookieName Cookie to delete
     *
     * @return bool|WP_ERROR true on success, else WP_ERROR
     */
    public static function DELETE(string $cookieName) : bool
    {
        $success = \setcookie($cookieName, false, time() -3600, $base);
        if (!$success) {
            return new \WP_ERROR(500, 'Cookie could not be deleted', $cookieName);
        }

        $_COOKIE[$cookieName] = false;

        return true;
    }
}