<?php

namespace TransientStorage\API;

use \TransientStorage\StorageHandlers\TransientHandler;
use \TransientStorage\StorageHandlers\CookieHandler;

class Rest extends \WP_REST_Controller
{
    /**
     * API Name
     *
     * @var string
     */
    protected $name;

    /**
     * Api version
     *
     * @var string
     */
    protected $version;

    /**
     * Api namespace
     *
     * @var string
     */
    protected $namespace;

    /**
     * Register routes when object created
     */
    public function __construct()
    {
        $this->name = 'ts';
        $this->version = 'v1';
        $this->namespace = $this->name . '/' . $this->version;

        $this->registerRoutes();
    }

    /**
     * Register rest routes
     *
     * @return void
     */
    protected function registerRoutes()
    {
        register_rest_route( $this->namespace, '/transient/(?P<transientID>[a-zA-Z0-9-]+)', array(
            array(
                'methods'   => \WP_REST_Server::READABLE,
                'callback'  => array( $this, 'fetchByTransientByID' ),
            ),
        ) );

        register_rest_route( $this->namespace, '/cookie/(?P<cookieName>[a-zA-Z0-9-]+)', array(
            array(
                'methods'   => \WP_REST_Server::READABLE,
                'callback'  => array( $this, 'fetchByCookieName' ),
            ),
        ) );
    }

    /**
     * Fetch transient data by transient ID
     *
     * @param \WP_REST_Request $request Request
     * @return \WP_ERROR|\WP_REST_Response Serialized josn on success, else WP_ERROR
     */
    public function fetchByTransientByID(\WP_REST_Request $request)
    {
        $params = $request->get_params();
        $transientID = $params['transientID'];
        $transientData = TransientHandler::FETCH($transientID);

        if (\is_wp_error($transientData)) {
            return \rest_ensure_response($transientData);
        }

        if (is_object($transientData)) {
            $transientData = $this->serializeObject($transientData);
        }

        return new \WP_REST_Response($transientData, 200);
    }

    /**
     * Fetch transient data by cookie name
     *
     * @param \WP_REST_Request $request Request
     * @return \WP_ERROR|\WP_REST_Response Serialized josn on success, else WP_ERROR
     */
    public function fetchByCookieName(\WP_REST_Request $request)
    {
        $params = $request->get_params();
        $cookieName = $params['cookieName'];

        $cookieData = CookieHandler::FETCH($cookieName);

        if (\is_wp_error($cookieData)) {
            return \rest_ensure_response($cookieData);
        }

        $transientData = TransientHandler::FETCH($cookieData);

        if (\is_wp_error($transientData)) {
            return \rest_ensure_response($transientData);
        }

        if (is_object($transientData)) {
            $transientData = $this->serializeObject($transientData);
        }

        return new \WP_REST_Response($transientData, 200);
    }

    /**
     * Serialize object to json data
     *
     * @param object $object Object to serialize
     *
     * @return array Return array
     */
    protected static function serializeObject(object $object) : array
    {
        $json = json_encode( (array)$object );

        return $json;
    }
}