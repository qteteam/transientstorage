<?php

namespace TransientStorage\StorageHandlers;

class TransientHandler
{
    
    /**
     * Create Transient
     *
     * @param string $transientID Transient ID to create
     * @param mixed $data         Data to store
     * @param integer $expiration Expiration time in seconds, 0 for infinite
     *
     * @return bool|WP_Error True on success, else WP_Error
     */
    public static function CREATE(string $transientID, $data, int $expiration = 60*60*12)
    {
        if (false == ($transientSuccess = \set_transient($transientID, $data, $expiration))) {
            return new \WP_Error( 500, 'Failed to create transient with ID', $transientID );
        }
        return true;
    }
    
    /**
     * Fetch transient data by ID
     *
     * @param string $transientID Transient id to fetch
     *
     * @return mixed Transient Data
     */
    public static function FETCH(string $transientID)
    {
        if (false == ($transientData = \get_transient($transientID))) {
            return new \WP_Error( 404, 'No transient with given ID exists', $transientID );
        }

        return $transientData;
    }

    /**
     * Update transient
     *
     * @param string $transientID Transient to update
     * @param mixed $data         New Data
     * @param integer $expiration New Expiration time
     *
     * @return bool|WP_Error True on success, WP_Error on failure
     */
    public static function UPDATE(string $transientID, $data, int $expiration = 60*60*12)
    {
        $transientData = self::FETCH($transientID);
        if (\is_wp_error($transientData)) {
            return $transientData;
        }

        $delete = self::DELETE($transientID);
        if (\is_wp_error($delete)) {
            return $delete;
        }

        $create = self::CREATE($transientID);
        if (\is_wp_error($create)) {
            return $create;
        }

        return true;
    }

    /**
     * Delete transient by ID
     *
     * @param string $transientID Transient to delete
     *
     * @return bool|WP_Error True on success, WP_Error on failure
     */
    public static function DELETE(string $transientID)
    {
        if (false == ($transientSuccess = \delete_transient($transientID))) {
            return new \WP_Error( 500, 'Failed to delete transient with given ID', $transientID );
        }

        return true;
    }
}